# Datse Multimedia Sites

This is for the general information about our Datse Multimedia Sites.

The intention of this is to:

* List the sites that we are responsible for
* List the policies that are applicable for sites
* List changes which are made to any of this
* List decisions which are made regarding moderation and changes to policy

## Sites responsible for

Our personal sites and sites we host for other people are somewhat different.
Some of our personal sites are things which are for the individuals within 
Datse Multimedia Productions, and some are part of what the business of 
Datse Mutlimedia Productsions does.  

There are also sites which we have created for others, sites we host for oters
and similar type things.  

### Datse Multimedia Sites 

These are the sites which are part of the business which is Datse Multimedia
Productions:

* Datse Multimedia Website
* [Datse Multimedia Social Site](https://gitlab.com/datse-multimedia-sites/sites/social/) (Mastodon)
* Open Psychology Chat service (Matrix)
* Datse Multimedia Video Site (PeerTube)
* Open Psychology Website

That's the quick list, more info will come up later.

### Personal Sites

These are sites that the administration is responsible for, but are of a more
personal nature.

* Jigme Datse Website

### Client Sites

These are sites which clients are the primary purpose of the sites

* TR By Hand Website

