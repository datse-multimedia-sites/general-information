# Contributing

There probably are a variety of ways that you can contribute to this project,
or the group that this project is part of.  We accept, "issues" from anyone
and don't really want to limit how people will submit, at least not in the 
sense of doing things which would limit the ability of people to be able to
submit issues, or other contributions.

So, right now I will list things that can help with this:

* Submitting issues
* Supporting us financially

I'm sure more will come in the future.  